/**************************************************************************************************
		PIN Configuration:
		P 0.0 : PWM 1 for left motor
		P 0.1 : PWM 3 for right motor
		P 0.2-0.5: GPIO for motors
		P 0.8: TX for UART 1
		P 0.14 :Encoder for left motor
		P 0.15 : Encoder for right motor
		
		
		Protocol:
		
		
	Bit 1:Bot id (1-5)
	Bit 2 :Direction of motion 
	
	IPADDR 122.38.0.150-55
********************************************************************************/ 
/*
TODO

deal with zero in ratio of vels


*/


#include  <lpc214x.h>		 //Includes LPC2148 register definitions
#include <stdio.h>

#define kpwm 1                    //kpwm the propotional constant value
#define BOT_ID 0
 /*clock settings of the microcontroller*/
#define Fosc            12000000                    
#define Fcclk           (Fosc * 5)                  
#define Fcco            (Fcclk * 4)                 
#define Fpclk           (Fcclk  ) * 1             
/*
Encoder interrupt values*/
volatile int leftenc=0;
volatile int rightenc=0;
volatile int leftvel=0;
volatile int rightvel=0;
volatile int lpwm,rpwm;
#define  UART_BPS	57600	 //Set Baud Rate hereno

__irq void T0ISR(void);
__irq void T1ISR(void);
void left_motor(void)__irq;
__irq void right_motor(void);
void forward();
void back();
void left();
void right();
void stop();
void pwm(int, int);
void pwm_char(char,char);

void  Init_UART1(void)					//This function setups UART1
{  
   unsigned int Baud16;
   U1LCR = 0x83;		            // DLAB = 1
   Baud16 = (Fpclk / 16) / UART_BPS;  
   U1DLM = Baud16 / 256;							
   U1DLL = Baud16 % 256;						
   U1LCR = 0x03;
}
				

void  UART1_SendByte(unsigned char data)	   //A function to send a byte on UART1
{  
   U1THR = data;				    
   while( (U1LSR&0x40)==0 );	    
}

char  UART1_ReceiveByte()	   //A function to send a byte on UART1
{  	
		char data;
   while( (U1LSR&0x01)==0 );
		data=U1RBR;
	return data;
}
void timer1init()
{
	PCONP|=(1<<2);
	T1CTCR=0;
	T1TC=0;
	T1PC=0;
	T1PR=0;
	T1MR1=3000000;// 5*6 * 10^6
	T1MCR|=(1<<3)|(1<<4);
	T1TCR=0x01;
	
}

void timerinit()
{
	PCONP|=0x01;
	T0CTCR=0;
	T0TC=0;
	T0PC=0;
	T0PR=0;
	T0MR0=10800000;
	T0MCR=0x03;
	T0TCR=0x01;
	
}
	
void pwminit()
{
	PINSEL0|=(1<<1)|(1<<3)|(1<<16)|(1<<18);
	PCONP|=(1<<5);	
	PWMTCR=0x02;				//Reset everything
	PWMTCR=0x05;
	PWMTC=0x0;						//Set timer to zero
	PWMPC=0x00;						//Prescaling set to zero
	PWMPR=0x09; 					//Maximum value of timer counter
	PWMPCR=0xA00; 				// Set PWM1 and PWM 3 as output enable
	PWMMR0=0x80;					//Max value to be reached
	PWMMCR=0x02;;					//pwmtc will reset when it reaches 128.(pwmmr0)
	PWMLER=0xA;						//set latch enable on pwm1 & pwm 3 
}

	void Enc1_init(void)
{

   EXTMODE |= (1<<1);         //Edge sensitive mode on EINT1
   EXTPOLAR |=(1<<1);           //RISING Edge Sensitive
   PINSEL0 |=(1<<6)|(1<<7); //Enable EINT2 on P0.3
   VICVectCntl3 = 0x20 |15; // 16 is index of EINT1
   VICVectAddr3 = (unsigned long)right_motor;
   VICIntEnable |= 1<<15;   //Enable EINT1
   EXTINT|=4;
}

	void Enc0_init(void)
{

   EXTMODE |= (1<<0);         //Edge sensitive mode on EINT0
   EXTPOLAR|=(1<<0);           //rISING Edge Sensitive
   PINSEL1 |= (1<<0); 			//Enable EINT0 on P0.16
   VICVectCntl2 = 0x20 |14; // 15 is index of EINT0
   VICVectAddr2 = (unsigned long)left_motor;
   VICIntEnable |= 1<<14;   //Enable EINT0
		EXTINT|=(1<<0);
}

 
void SetUpInt(void)
{
   VICVectAddr1 = (unsigned )T1ISR; //Pointer Interrupt Function (ISR)

    VICVectCntl1 = 0x20 | 5; //0x20 (i.e bit5 = 1) -> to enable Vectored IRQ slot
                               //0x4 (bit[5:0]) -> this the source number - here its timer1 which has VIC channel mask # as 5
                               //You can get the VIC Channel number from Lpc214x manual R2 - pg 58 / sec 5.5
    
    VICIntEnable |= (1<<5); //Enable timer1 int

    T1TCR = 0x02; //Reset Timer
	
   VICVectAddr0 = (unsigned )T0ISR; //Pointer Interrupt Function (ISR)

    VICVectCntl0 = 0x20 | 4; //0x20 (i.e bit5 = 1) -> to enable Vectored IRQ slot
                               //0x4 (bit[5:0]) -> this the source number - here its timer1 which has VIC channel mask # as 5
                               //You can get the VIC Channel number from Lpc214x manual R2 - pg 58 / sec 5.5
    
    VICIntEnable |= (1<<4); //Enable timer1 int

    T0TCR = 0x02; //Reset Timer
}

volatile int flag=0;
volatile int clear;
volatile	char k='P',p[4],q;
int main()
{  
	int i,j,k;	
	VPBDIV=0x01;														// Set PCLK at 12MHz i.e same as clock but PCLK is set at 60MHz
	PINSEL0|=(1<<1)|(1<<3)|(1<<16)|(1<<18)|(1<<14)|(1<<15); 	// Enable UART1 Rx and Tx pins &pwm 1 and pwm 3
	PINSEL1 |= (1<<0);
	PINSEL2 = 0x00000000;
	IO0DIR=(1<<12)|(1<<19)|(1<<18)|(1<<17);					// Set motor pins as outputs 

	Init_UART1();                          // Initialize UART1
	timerinit();
	timer1init();
	SetUpInt();
	T1TCR=0x02;
	T1TCR=0x01;
	UART1_SendByte('M');
	Enc1_init();
	Enc0_init();	
	pwminit();
	while(1)
	{
		
		while(k!=BOT_ID)
		{
		k=UART1_ReceiveByte();
		//	UART1_SendByte(k);
		}
		if( k == BOT_ID)
		{
			p[0]=UART1_ReceiveByte();
				if(p[0]=='F')
				{
					for (i=1;i<3;i++)
					{
						p[i]=UART1_ReceiveByte();
					}
					
					pwm_char(p[1],p[2]);
					forward();
					UART1_SendByte('D');
					T0TCR=0x02;
					T0TCR=0x01;
					
				}
				if(p[0]=='f')
				{
					for (i=1;i<3;i++)
					{
						p[i]=UART1_ReceiveByte();
					}
					forward();
					lpwm=p[1];
					rpwm=p[2];
					UART1_SendByte('D');
				T0TCR=0x02;
				T0TCR=0x01;
					
				}
				if(p[0]=='B')
				{
					for (i=1;i<3;i++)
					{
						p[i]=UART1_ReceiveByte();
					}
					
					pwm_char(p[1],p[2]);
					back();
					UART1_SendByte('D');
								T0TCR=0x02;
			T0TCR=0x01;
					
				}
				if(p[0]=='b')
				{
					for (i=1;i<3;i++)
					{
						p[i]=UART1_ReceiveByte();
					}
					back();
					lpwm=p[1];
					rpwm=p[2];
					UART1_SendByte('D');
				T0TCR=0x02;
				T0TCR=0x01;
					
				}
				if(p[0]=='L')
				{
					for (i=1;i<3;i++)
					{
						p[i]=UART1_ReceiveByte();
					}
					
					pwm_char(p[1],p[2]);
					left();
					UART1_SendByte('D');
								T0TCR=0x02;
			T0TCR=0x01;

				}
				if(p[0]=='l')
				{
					for (i=1;i<3;i++)
					{
						p[i]=UART1_ReceiveByte();
					}
					left();
					lpwm=p[1];
					rpwm=p[2];
					UART1_SendByte('D');
				T0TCR=0x02;
				T0TCR=0x01;
				
				}

				if(p[0]=='R')
				{
					for (i=1;i<3;i++)
					{
						p[i]=UART1_ReceiveByte();
					}
					
					pwm_char(p[1],p[2]);
					right();
					UART1_SendByte('D');
								T0TCR=0x02;
			T0TCR=0x01;

				}
				if(p[0]=='r')
				{
					for (i=1;i<3;i++)
					{
						p[i]=UART1_ReceiveByte();
					}
					right();
					lpwm=p[1];
					rpwm=p[2];
					UART1_SendByte('D');
				T0TCR=0x02;
				T0TCR=0x01;
				}
				if(p[0]=='w')
				{
					
					pwm_char(q,q);
					forward();
					UART1_SendByte('D');
								T0TCR=0x02;
								T0TCR=0x01;

				}
				if(p[0]=='s')
				{
					clear=IO0SET;
					IO0SET=IO0CLR;
					IO0CLR=clear;
					for(j=1;j<100;j++)
					{
						for(k=1;k<50;k++);
					}
					
					stop();
					UART1_SendByte('D');
					T0TCR=0x02;
			T0TCR=0x01;
				}
				if(p[0]=='a')
				{
					
					pwm_char(q,q);
					left();
					UART1_SendByte('D');
					T0TCR=0x02;
			T0TCR=0x01;
				
				}
				if(p[0]=='d')
				{
					
					pwm_char(q,q);
					right();
					UART1_SendByte('D');
					T0TCR=0x02;
			T0TCR=0x01;
				
				}
			  if(p[0]=='z')
				 {
					 
					 pwm_char(q,q);
					 back();
					 UART1_SendByte('D');
					 T0TCR=0x02;
			T0TCR=0x01;
				 }
				 if(p[0]=='p')
				{
					q=UART1_ReceiveByte();
					pwm_char(q,q);
					UART1_SendByte('D');
					T0TCR=0x02;
					T0TCR=0x01;
				}
				if(p[0]=='E')
				{
					UART1_SendByte('D');
					UART1_SendByte('D');
								T0TCR=0x02;
								T0TCR=0x01;
				}
		}
		
			
		}
		
}

void forward()
{
	IO0SET=(1<<17)|(1<<12);
	IO0CLR=(1<<18)|(1<<19);
}
void left()
{
	IO0SET=(1<<12)|(1<<18);
	IO0CLR=(1<<19)|(1<<17);
}
void right()
{
	IO0SET=(1<<19)|(1<<17);
	IO0CLR=(1<<12)|(1<<18);
}
void back()
{
	IO0CLR=(1<<17)|(1<<12);
	IO0SET=(1<<18)|(1<<19);
}
void stop()
{
	int k,l,m;
	k=IO0CLR;
	IO0CLR=IO0SET;
	IO0SET=k;
	
	for(l=0;l<1000;l++)
	{
		for (m=0;m<100;m++);
	}
	PWMMR1=0;
	PWMMR3=0;
	PWMLER=(1<<1)|(1<<3);
	IO0SET=(1<<17)|(1<<12)|(1<<18)|(1<<19);
	
}
void pwm(int p,int q)												//give left motor and right motor values between 0-128
{
	PWMMR1=p;
	PWMMR3=q;
	PWMLER=(1<<1)|(1<<3);
	k='P';
}
void pwm_char(char p,char q)												//give left motor and right motor values between 0-128
{
	//UART1_SendByte(p);
	//UART1_SendByte(q);
	PWMMR1= (int)p;
	PWMMR3= (int)q;
	PWMLER=(1<<1)|(1<<3);
	k='P';
}

__irq void T0ISR(void)
{

   T0IR |= (1<<0);	// Clear interrupt source		
	stop();
	UART1_SendByte('T');
   VICVectAddr = 0x0;           // Clear the priority hardware
}
__irq void T1ISR(void)
{	
	char x;
	float pwmratio,velratio,difference;
	int pwm1,pwm2;
	T1IR |= (1<<1);    //clear interrupt source
	UART1_SendByte('V');
	//	pwmratio=lpwm/(float)rpwm;
	leftvel=leftenc;
 	rightvel=rightenc;
//		velratio=leftvel/(float)rightvel;
//	difference=(velratio-pwmratio);
	leftenc=0; 
	rightenc=0;	
	/*{
		pwm1=lpwm;
		pwm2=rpwm+(kpwm*difference);
		pwm(pwm1,pwm2);
	}	*/	

	/*x=(leftvel>>24)&0xFF;
	UART1_SendByte(x);
	x=(leftvel>>16)&0xFF;
	UART1_SendByte(x);
	
	x=(leftvel>>8)&0xFF;
	UART1_SendByte(x);
	x=leftvel&0xFF;
	UART1_SendByte(x);
	*/
	
	
  VICVectAddr = 0x0;           // Clear the priority hardware
}

__irq void left_motor(void)
{
	
   leftenc+=1;							// add encoder value
	EXTINT|=(1<<0);	//clear interrupt 
	if(leftenc %10000==0)
	{
	//UART1_SendByte('L');
	}
		VICVectAddr=0; 	   // Clear the priority hardware
}

__irq void right_motor(void)
{
	
   rightenc+=1;							// add encoder value
	EXTINT|=4;		//clear interrupt 
	if(rightenc %10000==0)
	{
	//UART1_SendByte('R');
	}
	VICVectAddr=0; 	   // Clear the priority hardware
}

