# FIRA Robosoccer World Cup 2013 #

This is the repository for the electronics module of our team, which I developed jointly with Vasanth, Varun, Sanat.

We used TI's ez430-rf2500 modules and these codes are for the control and communication of the robosoccer robots we ingenuously developed at IIT Madras. I developed the drivers to facilitate the communication and control of robots with the decision making computer. The rest of the codes can be found at: https://github.com/VasanthKumar/FIRA.